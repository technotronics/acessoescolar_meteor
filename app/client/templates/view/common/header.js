Template.headerView.onRendered(function(){
	$('.ui.dropdown').dropdown();
});

Template.headerView.helpers({
	user: function(){
		var user = Meteor.user();
		if (user) {
			return {
				id: Meteor.userId(),
				email: user.emails[0].address
			};
		} else {
			return false;
		}
	},
	paginas: function() {
		return Pagina.find({}).fetch();
	}
});

Template.headerView.events({
	'click #loginBtn': function(event, template){
		 FlowRouter.go('loginView');
	},
	'click #logoutBtn': function(event, template){
		 Meteor.logout();
	}
});
