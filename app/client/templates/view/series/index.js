serieIdVar = new ReactiveVar(false);
turmaIdVar = new ReactiveVar(false);
turmaExcluirVar = new ReactiveVar(false);
serieExcluirVar = new ReactiveVar(false);0

Template.seriesView.onRendered(function(){
	$('.ui.dropdown').dropdown();

	var i18n = {
    previousMonth : 'Mês Anterior',
    nextMonth     : 'Próximo Mês',
    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubo','Novembro','Dezembro'],
    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sab']
	};
	var dtPresenca = new Pikaday({ field: $('#dtPresenca')[0],format:'DD/MM/YYYY',i18n:i18n });

});

Template.seriesView.events({
	"change #dtPresenca": function(event, template){
		dataPresencaVar.set(event.target.value);
	},
	"submit #seriesForm": function(event, template){
		 event.preventDefault();
		 let fields = $('#seriesForm').form('get values');
		 fields.serie = parseInt(fields.serie);
		 if (serieIdVar.get()) {
			 Serie.update(serieIdVar.get(),{$set:fields});
		 } else {
			 Serie.insert(fields);
		 }
		 $('#seriesForm').form('clear');
	},
	"submit #turmasForm": function(event, template){
		 event.preventDefault();
		 let fields = $('#turmasForm').form('get values');
		 if (turmaIdVar.get()) {
			 Turma.update(turmaIdVar.get(),{$set:fields});
		 } else {
			 Turma.insert(fields);
		 }
		 $('#turmasForm').form('clear');
	},
	"click .editTurmaBtn": function(){
		turmaIdVar.set(this._id);
		$('#turmasForm').form('set values', this);
	},
	"click .delTurmaBtn": function(){
		turmaIdVar.set(this._id);
		turmaExcluirVar.set(true);
	},
	"click #confSerieBtn": function(){
		Serie.remove(	serieIdVar.get());
		serieIdVar.set(false);
		serieExcluirVar.set(false);
	},
	"click .editSerieBtn": function(){
		serieIdVar.set(this._id);
		$('#seriesForm').form('set values', this);
	},
	"click .delSerieBtn": function(){
		serieIdVar.set(this._id);
		serieExcluirVar.set(true);
	},
	"click #confTurmaBtn": function(){
		Turma.remove(	turmaIdVar.get());
		turmaIdVar.set(false);
		turmaExcluirVar.set(false);
	},
	"click #cancelTurmaBtn": function(){
		turmaIdVar.set(false);
		turmaExcluirVar.set(false);
		$('#turmasForm').form('clear');
	},
	"click #cancelSerieBtn": function(){
		serieIdVar.set(false);
		serieExcluirVar.set(false);
		$('#seriesForm').form('clear');
	}
});

Template.seriesView.helpers({
	turmaExcluir: function(){
		return turmaExcluirVar.get();
	},
	serieExcluir: function(){
		return serieExcluirVar.get();
	},
	series: function(){
		let series = Serie.find({},{
			sort: {
				serie: 1
			}
		}).fetch();
		return series;
	}
});
