Template.presencasView.onRendered(function(){
	$('.ui.dropdown').dropdown();

	var i18n = {
    previousMonth : 'Mês Anterior',
    nextMonth     : 'Próximo Mês',
    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubo','Novembro','Dezembro'],
    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sab']
	};
	var dtPresenca = new Pikaday({ field: $('#dtPresenca')[0],format:'DD/MM/YYYY',i18n:i18n });
});
Template.presencasView.events({
	"click #delAllPresenca": function(event, template){
		 Meteor.call("delAllPresenca");
	}
});
Template.presencasView.helpers({
	totalPresencas: function() {
		return Presenca.find().count();
	},
	presencas: function() {
		return Presenca.find({},{
			sort: {
				date: -1,
				limit: 15
			}
		}).fetch();
	},
	series: function(){
		let series = Serie.find({},{
			sort: {
				serie: 1
			}
		}).fetch();
		return series;
	}
});
