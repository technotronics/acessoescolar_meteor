Template.loginView.onRendered(function(){
	$('.ui.form')
		.form({
			onFailure: function(formErrors, fields) {
				sAlert.error('Dados inválidos!');
				return false;
			},
			onSuccess: function(fields) {
				var fields = $('#loginForm').form('get values');
				 Meteor.loginWithPassword(
					 fields.email,
					 fields.password,
					 function(err) {
						 if (!err) {
							 FlowRouter.go('/');
							 sAlert.success('Usuário autenticado com sucesso!');
						 } else {
							 sAlert.error('Erro de autenticação!');
						 }
					 }
				);
				return false;
			},
			fields: {
				email: {
					identifier: 'email',
					rules: [
						{
							type   : 'email',
							prompt : 'Digite um email válido!'
						}
					]
				},
				password: {
					identifier: 'password',
					rules: [
						{
							type   : 'length[6]',
							prompt : 'Mínimo de 6 caracteres!'
						}
					]
				}
			},
			inline: true
		});
});
Template.loginView.events({
	'click #cadastreseBtn': function(event, template){
		var fields = $('form').form('get values');

		var userObject = {
			email: fields.email,
			password: fields.password
		};

		Accounts.createUser(userObject, function(){

		});
	}
});
