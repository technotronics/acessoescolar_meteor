Template.matriculaView.onRendered(function(){
		Meteor.subscribe("matriculas");
});

Template.matriculaView.helpers({
	isDisabled: function(){
		if (activeSerie.get() && activeTurma.get()) {
			return '';
		} else {
			return 'disabled';
		}
	},
	matriculas: function(){
		return Matricula.find({
			turma_id: activeTurma.get()
		}).fetch();
	}
});
