Template.seriesTurmasView.onRendered(function(){
	Meteor.subscribe("series");
	Meteor.subscribe("turmas");
});

Template.seriesTurmasView.helpers({
	series: function(){
		return Serie.find().fetch();
	},
	turmas: function(){
		return Turma.find({
			serie_id: activeSerie.get()
		}).fetch();
	}
});

Template.seriesTurmasView.events({
	'change #serie_id': function(event, template){
		 activeSerie.set(event.target.value);
	},
	'change #turma_id': function(event, template){
		 activeTurma.set(event.target.value);
	},
	'change #ano_id': function(event, template){
		 activeAno.set(event.target.value);
	}
});
