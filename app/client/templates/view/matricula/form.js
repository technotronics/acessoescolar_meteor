Template.matriculasFormView.helpers({
	isDisabled: function(){
		if (activeSerie.get() && activeTurma.get()) {
			return '';
		} else {
			return 'disabled';
		}
	},
	alunos: function() {
		return Aluno.find().fetch();
	}
});


Template.matriculasFormView.events({
	"submit #matriculaForm": function(event, template){
		 event.preventDefault();
		 var fields = $('#matriculaForm').form('get values');
		 fields.serie_id = activeSerie.get();
		 fields.turma_id = activeTurma.get();
		 console.log(fields);
		 Matricula.insert(fields);
		 $('#matriculaForm').form('clear');
	}
});
