Template.paisFormView.helpers({

});

Template.paisFormView.events({
	"submit #paiForm": function(event, template){
		 event.preventDefault();
		 let fields = $('#paiForm').form('get values');
		 fields.aluno_id = FlowRouter.getParam('id');
		 Pai.insert(fields);
		 $('#paiForm').form('clear');
	}
});
