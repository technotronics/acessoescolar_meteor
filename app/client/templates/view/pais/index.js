Template.paisView.onRendered(function(){
	Meteor.subscribe("alunos");
	Meteor.subscribe("pais");
});

Template.paisView.helpers({
	pais: function() {
		return Pai.find({
			aluno_id: FlowRouter.getParam('id')
		},{
			sort: {
				nome: 1
			}
		}).fetch();
	}
});

Template.paisView.events({
	"click .delPai": function(event, template){
		 var pai = Pai.findOne(this._id);
		 Pai.remove(this._id);
	}
});
