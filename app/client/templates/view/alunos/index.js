Template.alunosView.onRendered(function(){
	Meteor.subscribe("alunos");
	Meteor.subscribe("fotosAlunos");
});

Template.alunosView.helpers({
	alunos: function() {
		return Aluno.find({
		},{
			sort: {
				nome: 1
			}
		}).fetch();
	}
});

Template.alunosView.events({
	"click .delAluno": function(event, template){
		 var aluno = Aluno.findOne(this._id);
		 FotoAluno.remove({
			 _id: aluno.foto_id
		 });
		 Aluno.remove(this._id);
	}
});
