Template.alunosFormView.helpers({
	alunoFotoUp: function() {
		return {
			finished: function(index, fileInfo, context) {
				fotoUploadIdVar.set(FotoAluno.insert(fileInfo));
			}
		}
	},
	alunoFotoAtual: function() {
		return FotoAluno.findOne(fotoUploadIdVar.get());
	}
});

Template.alunosFormView.events({
	"submit #alunoForm": function(event, template){
		 event.preventDefault();
		 let fields = $('#alunoForm').form('get values');
		 fields.foto_id = fotoUploadIdVar.get();
		 Aluno.insert(fields);
		 $('#alunoForm').form('clear');
		 fotoUploadIdVar.set(false);
	}
});
