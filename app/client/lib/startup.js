// Client startup
Meteor.startup(function(){

	// SAlert Config
	sAlert.config({
		effect: 'stackslide',
		position: 'top',
		timeout: 5000,
		html: false,
		onRouteClose: false,
		stack: true
	});

	// Callback de logout
	Tracker.autorun(function(){
		// Sempre que o ID do usuario logado mudar, o código abaixo será executado.
		if (!Meteor.userId()) {
			FlowRouter.go('homeView');
		}
	});

	dataPresencaVar = new ReactiveVar(new Date());
	fotoUploadIdVar = new ReactiveVar();

	activeSerie = new ReactiveVar();
	activeTurma = new ReactiveVar();
	activeAno = new ReactiveVar();

	//uploads
	Uploader.uploadUrl = Meteor.absoluteUrl("upload"); // Cordova needs absolute URL
	Uploader.localisation = {
		browse: "Procurar",
		cancelled: "Cancelado",
		remove: "Remover",
		upload: "Upload",
		done: "Feito",
		cancel: "Cancelar"
	}
});
