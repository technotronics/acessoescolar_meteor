Template.registerHelper("getSerie", function(aluno_id){
	var matricula = Matricula.findOne({aluno_id:aluno_id});
	if (!matricula) return '';
	var serie = Serie.findOne(matricula.serie_id);
	if (serie)
		return serie.nome;
	else return '';
});

Template.registerHelper("getTurma", function(aluno_id){
	var matricula = Matricula.findOne({aluno_id:aluno_id});
	if (!matricula) return '';
	var turma = Turma.findOne(matricula.turma_id);
	console.log(turma);
	if (turma)
		return turma.nome;
	else return '';
});

Template.registerHelper("presentesClass", function(val1, val2, cor1, cor2){
	if (val1 == val2) return cor1;
	else return cor2;
});
