Template.registerHelper("isAdmin", function(){
	if (Roles.userIsInRole(Meteor.userId(), ['admin'])) {
		return true;
	} else {
		return false
	}
});
