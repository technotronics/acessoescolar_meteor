// Server startup
Meteor.startup(function(){
	// Verifica se há usuarios administradores
	var admins = Roles.getUsersInRole('admin').fetch();
	_.each(admins, function(admin){
		console.log('Encontrado admin '+admin.profile.name);
	});

	if (admins.length == 0) { // se não há
		// lista de usuaarios administadores a serem criados
		var users = [
			{
				name:"Administrador",
				email:"acesso@technotronics.cc"
			}
		];
		// para cada usuario
		_.each(users, function (user) {
			var id;
			// criar usuario
			id = Accounts.createUser({
				email: user.email,
				password: "123123",
				profile: { name: user.name }
			});
			// adicionar role admin ao usuario criado
			Roles.addUsersToRoles(id, 'admin');
			console.log('Criado admin '+user.name+'['+user.email+']');
		});
	}

	// Uploads
	UploadServer.init({
		tmpDir: process.env.PWD + '/.uploads/tmp',
		uploadDir: process.env.PWD + '/.uploads/alunos/',
		checkCreateDirectories: true, //create the directories for you
		imageVersions: {
			fotoCracha: {width: 300, height: 400 }
		}
	});
});
