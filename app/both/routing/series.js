// Rotas com Autenticação
import { privateRoutes } from './groups.js'

privateRoutes.route('/series',{
	name: 'seriesView',
	action: function() {
		BlazeLayout.render('adminLayout', {
			header: 'headerView',
			main: 'seriesView'
		});
	}
});
