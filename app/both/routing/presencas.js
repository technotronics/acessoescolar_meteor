// Rotas com Autenticação
import { privateRoutes } from './groups.js'

privateRoutes.route('/presencas',{
	name: 'presencasView',
	action: function() {
		BlazeLayout.render('adminLayout', {
			header: 'headerView',
			main: 'presencasView'
		});
	}
});
