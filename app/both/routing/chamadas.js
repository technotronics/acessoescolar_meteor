// Rotas com Autenticação
import { privateRoutes } from './groups.js'

privateRoutes.route('/chamada',{
	name: 'chamadaView',
	action: function() {
		BlazeLayout.render('adminLayout', {
			header: 'headerView',
			main: 'chamadaView'
		});
	}
});
