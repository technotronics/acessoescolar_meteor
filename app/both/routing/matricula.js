// Rotas com Autenticação
import { privateRoutes } from './groups.js'

privateRoutes.route('/matricula',{
	name: 'matriculaView',
	action: function() {
		BlazeLayout.render('adminLayout', {
			header: 'headerView',
			main: 'matriculaView'
		});
	}
});
