// Rotas com Autenticação
import { privateRoutes } from './groups.js'

privateRoutes.route('/alunos',{
	name: 'alunosView',
	action: function() {
		BlazeLayout.render('adminLayout', {
			header: 'headerView',
			main: 'alunosView'
		});
	}
});

privateRoutes.route('/alunos/form',{
	name: 'alunosFormView',
	action: function() {
		BlazeLayout.render('adminLayout', {
			header: 'headerView',
			main: 'alunosFormView'
		});
	}
});
privateRoutes.route('/alunos/form/:aluno_id',{
	name: 'alunosFormView',
	action: function() {
		BlazeLayout.render('adminLayout', {
			header: 'header',
			main: 'alunosFormView'
		});
	}
});
