// Rotas com Autenticação
import { privateRoutes } from './groups.js'

privateRoutes.route('/pais/:id',{
	name: 'paisView',
	action: function() {
		BlazeLayout.render('adminLayout', {
			header: 'headerView',
			main: 'paisView'
		});
	}
});
