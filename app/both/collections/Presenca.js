Presenca = new Mongo.Collection("presencas");
Presenca.helpers({
	aluno: function() {
		return Aluno.findOne({
			uid: this.uid
		});
	}
});
Presenca.allow({
	insert: function(){
		return true;
	},
	update: function(){
		return true;
	},
	remove: function(){
		return true;
	}
});
