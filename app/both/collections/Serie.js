Serie = new Mongo.Collection("series");

Serie.helpers({
	turmas: function(){
		return Turma.find({serie_id:this._id}).fetch();
	}
});

Serie.allow({
	insert: function(){
		return true;
	},
	update: function(){
		return true;
	},
	remove: function(){
		return true;
	}
});
