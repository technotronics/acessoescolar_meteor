Matricula = new Mongo.Collection("matriculas");
Matricula.helpers({
	aluno: function() {
		return Aluno.findOne(this.aluno_id);
	},
	serie: function() {
		return Serie.findOne(this.serie_id);
	},
	turma: function() {
		return Turma.findOne(this.turma_id);
	}
});
Matricula.allow({
	insert: function(){
		return true;
	},
	update: function(){
		return true;
	},
	remove: function(){
		return true;
	}
});
