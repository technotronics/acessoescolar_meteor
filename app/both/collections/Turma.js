Turma = new Mongo.Collection("turmas");
Turma.helpers({
	qtd_alunos: function(){
		return Matricula.find({
			turma_id: this._id
		}).count();
	},
	qtd_presentes: function(){
		var matriculas = Matricula.find({
			turma_id: this._id
		}).fetch();
		var uids = [];
		_.each(matriculas, function(matricula){
			uids.push(matricula.aluno().uid);
		});
		return Presenca.find({
			uid: { $in: uids }
		}).count();
	}
});
Turma.allow({
	insert: function(){
		return true;
	},
	update: function(){
		return true;
	},
	remove: function(){
		return true;
	}
});
