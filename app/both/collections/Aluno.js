Aluno = new Mongo.Collection("alunos");
Aluno.helpers({
	foto: function() {
		return FotoAluno.findOne(this.foto_id);
	},
	matricula: function(){
		return Matricula.findOne({aluno_id:this._id});
	},
	pais: function(){
		return Pai.find({aluno_id: this._id}).fetch();
	}
});
Aluno.allow({
	insert: function(){
		return true;
	},
	update: function(){
		return true;
	},
	remove: function(){
		return true;
	}
});
